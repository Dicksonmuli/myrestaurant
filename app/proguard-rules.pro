# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# Keep annotations as they are used internally for fabric / crashlytics
#-keepattributes *Annotation*
# Keep line numbers
-keepattributes SourceFile,LineNumberTable
# Keep all exception code deobfuscated
-keep public class * extends java.lang.Exception
# Keep all crashlytics files deobfuscated
#-keep class com.crashlytics.** { *; }
#-dontwarn com.crashlytics.**
# Keep all objects
#-keep class com.coop.app.objects.** { *; }
# For Jackson 2.x (fasterxml)
-keep class com.fasterxml.jackson.databind.ObjectMapper {
    public <methods>;
    protected <methods>;
}
-keep class com.fasterxml.jackson.databind.ObjectWriter {
    public ** writeValueAsString(**);
}
# For the animation library
-keep class com.daimajia.easing.** { *; }
-keep interface com.daimajia.easing.** { *; }

-keepclassmembers class * {
     @com.fasterxml.jackson.annotation.JsonCreator *;
     @com.fasterxml.jackson.annotation.JsonProperty *;
}
-dontwarn com.fasterxml.jackson.databind.**
# Don't obfuscate class names as reflection from annotations won't work
-keepnames class * { *; }
# Remove log messages
-assumenosideeffects class android.util.Log {
    public static *** v(...);
    public static *** i(...);
    public static *** w(...);
    public static *** d(...);
    public static *** e(...);
    public static boolean isLoggable(java.lang.String, int);
}
