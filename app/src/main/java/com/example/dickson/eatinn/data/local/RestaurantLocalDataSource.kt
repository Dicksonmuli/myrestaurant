package com.example.dickson.eatinn.data.local

import com.example.dickson.eatinn.data.Restaurant
import com.example.dickson.eatinn.data.remote.RestaurantDataSource
import com.example.dickson.eatinn.others.AppExecutors

class RestaurantLocalDataSource private constructor(
        val appExecutors: AppExecutors,
        val restaurantDAO: RestaurantDao) : RestaurantDataSource {

    private val TAG = javaClass.simpleName

    /**
     * Note: [LoadCountriesCallback.onDataNotAvailable] is fired if the database doesn't exist
     * or the table is empty.
     */
    override fun getRestaurants(callback: RestaurantDataSource.LoadRestaurantsCallback) {
        appExecutors.diskIO.execute {

            // Fetch Restaurants from the database
            val restaurants = restaurantDAO.getRestaurants()

            appExecutors.mainThread.execute {

                // We did not fetch any countries
                if (restaurants.isEmpty()) {

                    // This will be called if the table is new or just empty.
                    callback.onDataNotAvailable()

                } else {

                    // This will be called if we are able to fetch countries from the database.
                    callback.onRestaurantsLoaded(restaurants)
                }
            }
        }

    }

    /**
     * Get restaurant by id
     */
    override fun getRestaurant(id: String, callback: RestaurantDataSource.GetRestaurantCallback) {
        appExecutors.diskIO.execute {
            val restaurant = restaurantDAO.getRestaurantById(id)

            appExecutors.mainThread.execute {
                if (restaurant != null) {
                    callback.onRestaurantLoaded(restaurant)
                } else {
                    callback.onDataNotAvailable()
                }
            }
        }
    }

    override fun getStarredRestaurants(callback:  RestaurantDataSource.LoadRestaurantsCallback) {
        appExecutors.diskIO.execute {
            // Fetch Starred Countries from the database
            val restaurants = restaurantDAO.getStarredRestaurants()

            appExecutors.mainThread.execute {
                // We did not fetch any countries
                if (restaurants.isEmpty()) {

                    // This will be called if the table is new or just empty.
                    callback.onDataNotAvailable()

                } else {
                    // This will be called if we are able to fetch countries from the database.
                    callback.onRestaurantsLoaded(restaurants)
                }
            }
        }

    }

    override fun saveRestaurant(restaurant: Restaurant) {
        appExecutors.diskIO.execute{restaurantDAO.insertRestaurant(restaurant)}
    }

    override fun updateRestaurant(restaurant: Restaurant) {
        appExecutors.diskIO.execute {restaurantDAO.updateRestaurant(restaurant)}
    }

    companion object {
        private var INSTANCE: RestaurantLocalDataSource? = null

        @JvmStatic
        fun getInstance(appExecutors: AppExecutors, restaurantDao: RestaurantDao): RestaurantLocalDataSource {
            if (INSTANCE == null) {
                synchronized(RestaurantLocalDataSource::javaClass) {
                    INSTANCE = RestaurantLocalDataSource(appExecutors, restaurantDao)
                }
            }
            return INSTANCE!!
        }
    }
}