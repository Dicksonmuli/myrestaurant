package com.example.dickson.eatinn.interfaces.callbacks;

import com.android.volley.VolleyError;

/**
 * Created by dickson-incentro on 2/26/18.
 */

public interface VolleyCallback<T> {

    /**
     * Callback for when result is back, from volley
     *
     * @param response     response
     * @param error        error
     * @param errorMessage errorMessage
     */
    void getResult(T response, VolleyError error, String errorMessage);
}
