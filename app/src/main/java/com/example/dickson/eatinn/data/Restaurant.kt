package com.example.dickson.eatinn.data

import android.arch.persistence.room.*
import android.support.annotation.NonNull
import com.example.dickson.eatinn.objects.Category
import com.example.dickson.eatinn.objects.Location
import java.io.Serializable
import java.util.*


/**
 * Immutable model class for a Restaurant. In order to compile with Room, we can't use @JvmOverloads to generate multiple constructors.
 * id of the task
 */

// as it denotes, is using the table name called “restaurants”. If name is not specified, by default class name is used as the Table name.
@Entity(tableName = "restaurants")
data class Restaurant constructor(

        @NonNull @PrimaryKey @ColumnInfo(name = "id")
        var id: String = "",
        @ColumnInfo(name = "name")
        var name: String = "",
        @ColumnInfo(name = "image_url")
        var imageUrl: String = "",
        @ColumnInfo(name = "is_closed")
        var isClosed: Boolean = false,
        @ColumnInfo(name = "url")
        var url: String = "",
        @ColumnInfo(name = "review_count")
        var reviewCount: Int = 0,
        @ColumnInfo(name = "categories")
        var categories: ArrayList<Category> = ArrayList(),
        @ColumnInfo(name = "rating")
        var rating: Double = 0.0,
        @ColumnInfo(name = "starred")
        var isStarred: Boolean = false,
        @ColumnInfo(name = "price")
        var price: String = "",
        @Embedded(prefix = "location_")
        var location: Location,
        @ColumnInfo(name = "phone")
        var phone: String = "",
        @ColumnInfo(name = "longitude")
        var longitude: Double = 0.0,
        @ColumnInfo(name = "latitude")
        var latitude: Double = 0.0,
        @ColumnInfo(name = "address")
        var address: String = ""
) : Serializable