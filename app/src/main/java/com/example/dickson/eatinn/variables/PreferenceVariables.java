package com.example.dickson.eatinn.variables;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by dickson-incentro on 2/10/18.
 */

public class PreferenceVariables {

    public static final String ONBOARDING_VERSION_SEEN = "onboarding_version";

    public static final String USER_LOGGED_IN= "user_logged_in";

    /**
     * Removes all shared preferences from the device
     *
     * @param context context
     */
    public static void removePreferencesFromDevice(Context context) {
        if (context != null) {
            SharedPreferences sp = context.getSharedPreferences(context.getApplicationContext().getPackageName(), Context.MODE_PRIVATE);
            sp.edit().clear().apply();
        }
    }
}
