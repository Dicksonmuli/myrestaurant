package com.example.dickson.eatinn.utils;

import android.util.Patterns;

/**
 * Created by dickson-incentro on 3/7/18.
 */

public class Validation {
    /**
     * validating registration credentials
     * validation methods to ensure our users' credentials are valid
     * and reasonably secure, and error handling if they are not.
     */
    public static boolean isValidEmail(String email) {
        boolean isGoodEmail = (email != null && Patterns.EMAIL_ADDRESS.matcher(email).matches());
        if (!isGoodEmail) {
            return false;
        }
        return isGoodEmail;
    }
    public static boolean isValidName(String name) {
        if (name.equals("")) {
            return false;
        }
        return true;
    }
    public static boolean isValidPassword(String password, String confirmPassword) {
        if (password.length() < 6) {
            return false;
        }else if (!password.equals(confirmPassword)) {
            return false;
        }
        return true;
    }

    public static boolean isValidPassword(String password) {
        if (password.length() < 6) {
            return false;
        }
        return true;
    }

}
