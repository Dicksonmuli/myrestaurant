package com.example.dickson.eatinn.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.example.dickson.eatinn.variables.PreferenceVariables;

/**
 * Created by dickson-incentro on 3/5/18.
 */

public class LoginUtils {
    private static final String TAG = LoginUtils.class.getSimpleName();

    public static boolean isLoggedIn(SharedPreferences sp) {
        return !TextUtils.isEmpty(sp.getString(PreferenceVariables.USER_LOGGED_IN, ""));
    }

    /**
     * Obtain the user name
     *
     * @param context context
     * @return String
     */
    public static String getLoggedInUser(Context context) {
        if (context == null) {
            Log.e(TAG, " getSelectedStore called on null context. ");
            return "";
        }

        SharedPreferences sp = context.getSharedPreferences(context.getApplicationContext().getPackageName(), Context.MODE_PRIVATE);

        return getLoggedUser(sp);
    }

    /**
     * Obtain the user name
     *
     * @param preferences preferences
     * @return String
     */
    public static String getLoggedUser(SharedPreferences preferences) {
        if (preferences == null) {
            Log.e(TAG, " getSelectedStore called on null SharedPreferences. ");
            return "";
        }

        return preferences.getString(PreferenceVariables.USER_LOGGED_IN, "");
    }

    /**
     *
     * @param name logged in user
     */
    public static void setLoggedInUser(String name, SharedPreferences preferences, Context context) {
        if (preferences == null || context == null) {
            Log.e(TAG, " setSelectedStore called on null SharedPreferences/context. ");
            return;
        }

        preferences.edit().putString(PreferenceVariables.USER_LOGGED_IN, name).apply();
    }

}
