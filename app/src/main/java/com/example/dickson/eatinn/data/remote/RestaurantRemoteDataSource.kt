package com.example.dickson.eatinn.data.remote

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.example.dickson.eatinn.data.Restaurant
import com.example.dickson.eatinn.network.NetworkManager
import com.example.dickson.eatinn.network.NetworkVariables
import com.example.dickson.eatinn.objects.RestaurantObject
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.IOException
import java.util.*

class RestaurantRemoteDataSource(val mContext: Context) : RestaurantDataSource {

    // Tag for logging
    private val TAG = javaClass.simpleName

    override fun getRestaurants(callback: RestaurantDataSource.LoadRestaurantsCallback) {

        val url = NetworkVariables.getUrlWithSearchTerm("london")
        NetworkManager.getInstance(mContext).getCall(url, null) { response, error, errorMessage ->
            if (!TextUtils.isEmpty(response)) {
                try {
                    val objectMapper = ObjectMapper()
                    val jsonNode = objectMapper.readTree(response)

                    if (jsonNode.get("businesses") != null) {
                        val restaurantObject = jsonNode.get("businesses")

                        val restaurantResult = objectMapper.treeToValue(restaurantObject, Array<RestaurantObject>::class.java)

                        // cast the array to an array list
                        val restaurantsFromRemote = Arrays.asList(*restaurantResult)

                        // restaurants on local
                        val restaurantsListForLocal = mutableListOf<Restaurant>()

                        // iterate through restaurant
                        for (i in restaurantsFromRemote.indices) {

                            // get a restaurant at position i
                            val restaurant = restaurantsFromRemote[i]

                            // israted
                            var isRated = false;

                            if (restaurant.rating > 0) {
                                isRated = true;
                            }

                            if (restaurant.address == null) {
                                restaurant.address = ""
                            }

                            // Create an instance of RestaurantForLocal
                            val restaurantForLocal = Restaurant(
                                    restaurant.id,
                                    restaurant.name,
                                    restaurant.imageUrl,
                                    restaurant.isClosed,
                                    restaurant.url,
                                    restaurant.reviewCount,
                                    restaurant.categories,
                                    restaurant.rating,
                                    isRated,
                                    restaurant.price,
                                    restaurant.location,
                                    restaurant.phone,
                                    restaurant.longitude,
                                    restaurant.latitude,
                                    restaurant.address
                            )

                            restaurantsListForLocal.add(restaurantForLocal)
                        }

                        callback.onRestaurantsLoaded(restaurantsListForLocal)
                    }
                } catch (e: IOException) {
                    Log.e(TAG, "error: " + e.toString())
                }

            } else {
                // send a callback to show tha the data was not available
                callback.onDataNotAvailable()
            }
        }
    }

    override fun getStarredRestaurants(callback: RestaurantDataSource.LoadRestaurantsCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getRestaurant(id: String, callback: RestaurantDataSource.GetRestaurantCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveRestaurant(restaurant: Restaurant) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateRestaurant(restaurant: Restaurant) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}