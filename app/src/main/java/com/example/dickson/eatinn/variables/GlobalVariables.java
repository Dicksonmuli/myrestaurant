package com.example.dickson.eatinn.variables;

import com.example.dickson.eatinn.BuildConfig;

/**
 * Created by dickson-incentro on 2/10/18.
 */

public class GlobalVariables {

    /**
     * Environment (SET FROM GRADLE FILE)
     */
    public static final boolean TEST_ENABLED = BuildConfig.TEST;
    public static final boolean ACC_ENABLED = BuildConfig.ACC;
    public static final boolean ERASE_PREFS = BuildConfig.ERASEPREFS;

    /**
     * Coop settings
     */
    public static final int WISHLIST_MAX_PRODUCTS = 130;

    /**
     * Request codes
     */
    public static final int REQUESTCODE_SEARCH = 10;
    public static final int REQUESTCODE_USER_ACCOUNT = 13;

    /**
     * Intent extra names
     */
    public static final String INTENT_SEARCH_HASOPTIONSMENU = "search_options_menu";
    public static final String INTENT_SEARCH_RESULT = "search_result";
    public static final String INTENT_SEARCH_CANCELED = "search_canceled";
    public static final String INTENT_STORE_CHANGED = "search_canceled";
    public static final String INTENT_IS_LOGGED_IN = "is_logged_in";

    //        a constant to save to shared preferences
    public static final String PREFERENCES_LOCATION_KEY = "location";

    public static final String FIREBASE_CHILD_SEARCHED_LOCATION = "searchedLocation";
    public static final String FIREBASE_CHILD_RESTAURANTS = "restaurants";
    //        index string constant to reference the 'index' key of Restaurant objects
    public static final String FIREBASE_QUERY_INDEX = "index";

    public static final String EXTRA_KEY_POSITION = "position";
    public static final String EXTRA_KEY_RESTAURANTS = "restaurants";
    // navigation constants
    public static final String KEY_SOURCE = "source";
    public static final String SOURCE_SAVED ="saved";
    public static final String SOURCE_FIND = "find";
}
