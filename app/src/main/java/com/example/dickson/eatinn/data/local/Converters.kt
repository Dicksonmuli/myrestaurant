package com.example.dickson.eatinn.data.local

import android.arch.persistence.room.TypeConverter
import com.example.dickson.eatinn.objects.Category
import com.google.gson.reflect.TypeToken
import com.google.gson.Gson

class Converters {

    /**
     * This method takes our arraylist object as parameter and returns string representation for it so that it can be stored in Room Database.
     * Very simple and easy way to convert any object to string is converting it into its JSON equivalent. Just creating Gson object and calling toJson method with our object as parameter is enough.
     */
    @TypeConverter
    fun fromCategoriesToJson(categories: ArrayList<Category>?): String? {
        if (categories == null) {
            return null
        }

        // todo this code is OK but it uses object mapper
//        val mapper = ObjectMapper()
//
//        // Convert List of Categories objects to JSON
//        return mapper.writeValueAsString(categories)

        // using Gson
        val gson = Gson()
        val type = object : TypeToken<List<Category>>() {

        }.type

        return gson.toJson(categories, type)
    }

    /**
     * While reading data back from Room Database, we get JSON form of our arraylist which we need to convert back.
     */
    @TypeConverter
    fun toCategoriesList(categoryString: String?): ArrayList<Category>? {

        // Initialize ArrayList, this will be the list we return from this method
        var list: ArrayList<Category> = ArrayList()

        if (categoryString == null) {
            return null
        }

        // todo this code is OK but it uses object mapper
//        val objectMapper = ObjectMapper()
//
//        val typeReference = TypeFactory.defaultInstance().constructCollectionType(ArrayList::class.java, Category::class.java)
//
//        try {
//            list = objectMapper.readValue<ArrayList<Category>>(category, typeReference)
//        } catch (e: IOException) {
//        }
//
//        return list

        val gson = Gson()
        val type = object : TypeToken<List<Category>>() {

        }.type

        return gson.fromJson(categoryString, type)
    }
}
