package com.example.dickson.eatinn.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.dickson.eatinn.R;
import com.example.dickson.eatinn.data.Restaurant;
import com.example.dickson.eatinn.fragments.RestaurantDetailFragment;
import com.example.dickson.eatinn.interfaces.NavigationInteraction;
import com.example.dickson.eatinn.objects.RestaurantObject;

import java.util.List;

/**
 * Created by dickson-incentro on 3/10/18.
 */

public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.ViewHolder> {

    private NavigationInteraction mFragmentNavigation;
    private Activity mActivity;
    private List<Restaurant> mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View root;
        private ImageView imageView;
        private TextView titleTextView;
        private TextView locationTextView;
        private TextView ratingTextView;
        private TextView cuisineTextView;


        public ViewHolder(View view) {
            super(view);
            root = view;
            imageView = view.findViewById(R.id.image_view);
            titleTextView = view.findViewById(R.id.title_tv);
            locationTextView = view.findViewById(R.id.location_tv);
            ratingTextView = view.findViewById(R.id.rating_tv);
            cuisineTextView = view.findViewById(R.id.cuisine_tv);
        }
    }

    /**
     * Constructor
     *
     * @param activity activity
     * @param fragmentNavigation fragmentNavigation
     * @param dataset mDataset
     */
    public RestaurantListAdapter(Activity activity, NavigationInteraction fragmentNavigation, List<Restaurant> dataset) {
        mActivity = activity;
        mDataset = dataset;
        mFragmentNavigation = fragmentNavigation;
    }

    /**
     * Create new view
     *
     * @param parent parent
     * @param viewType viewType
     * @return new view holder
     */
    @Override
    public RestaurantListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_restaurant, parent, false);

        RestaurantListAdapter.ViewHolder newHolder = new RestaurantListAdapter.ViewHolder(itemView);
        return newHolder;
    }

    /**
     * Replace the contents of the view
     *
     * @param holder holder
     * @param  position position
     */
    @Override
    public void onBindViewHolder(RestaurantListAdapter.ViewHolder holder, int position) {
        final Restaurant item = mDataset.get(position);

        holder.titleTextView.setText(item.getName());
        holder.ratingTextView.setText(String.valueOf(item.getRating()));
        holder.locationTextView.setText(item.getLocation().getCountry());
        // holder.cuisineTextView.setText(item.getReviewCount());

        // Request options
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.fitCenter();

        // set the lister icon/image
        Glide.with(mActivity)
                .setDefaultRequestOptions(requestOptions)
                .load(item.getImageUrl())
                .into(holder.imageView);

        // set onclick listener
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String contentPageTitle = item.getName();

                // push content page fragment
                mFragmentNavigation.pushFragment(RestaurantDetailFragment.newInstance(contentPageTitle));
            }
        });
    }

    /**
     * Return the size of the dataset
     * @return int representing the dataset
     */
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) { return position; }
}
