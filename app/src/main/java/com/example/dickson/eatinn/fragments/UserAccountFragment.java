package com.example.dickson.eatinn.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.dickson.eatinn.R;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by dickson-incentro on 3/8/18.
 */

public class UserAccountFragment extends BaseFragment implements View.OnClickListener{

    private final String TAG = getClass().getSimpleName();
    FirebaseAuth.AuthStateListener mAuthListener;

    private FirebaseAuth mAuth;
    private RelativeLayout goToFavorites;
    private RelativeLayout mLogout;

    private String mName;

    private SharedPreferences preferences;
    /**
     * OnCreateView, inflates layout
     *
     * @param inflater           inflater
     * @param container          container
     * @param savedInstanceState savedInstanceState
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_account, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        goToFavorites = view.findViewById(R.id.action_favorites);
        mLogout = view.findViewById(R.id.action_logout);

        goToFavorites.setOnClickListener(this);
        mLogout.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
        preferences = getActivity().getSharedPreferences(getContext().getPackageName(), Context.MODE_PRIVATE);
    }
    @Override
    public void onStart() {
        super.onStart();
//        try {
//            mAuth.addAuthStateListener(mAuthListener);
//        }catch (NullPointerException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onStop() {
        super.onStop();
//        if (mAuthListener != null) {
//            mAuth.removeAuthStateListener(mAuthListener);
//        }
    }

    @Override
    public void onClick(View view) {
        if (view == goToFavorites) {
            // go to wish list fragment
            WishListFragment wishListFragment = new WishListFragment();
            if (mFragmentNavigation != null) {
                mFragmentNavigation.pushFragment(wishListFragment);
            }
        }

        if (view == mLogout) {

        }
    }
}
