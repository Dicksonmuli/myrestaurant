package com.example.dickson.eatinn.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dickson.eatinn.R;
import com.example.dickson.eatinn.utils.LoginUtils;
import com.example.dickson.eatinn.utils.Validation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by dickson-incentro on 3/5/18.
 */

public class LoginFragment extends BaseFragment implements View.OnClickListener{

    private final String TAG = getClass().getSimpleName();

    // views
    Button mPasswordLoginButton;
    EditText mEmailEditText;
    EditText mPasswordEditText;
    TextView mRegisterTextView;

    private String mEmailAdrress;
    private String mPassword;

    // Firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ProgressDialog mAuthProgressDialog;

    private SharedPreferences preferences;

    public LoginFragment() {
        // Required empty public constructor
    }
    /**
     * OnCreateView, inflates layout
     *
     * @param inflater           inflater
     * @param container          container
     * @param savedInstanceState savedInstanceState
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEmailEditText = view.findViewById(R.id.email);
        mPasswordEditText = view.findViewById(R.id.password);
        mPasswordLoginButton = view.findViewById(R.id.login_button);
        mRegisterTextView = view.findViewById(R.id.registerTextView);

        mAuth = FirebaseAuth.getInstance();
        mRegisterTextView.setOnClickListener(this);
        createAuthProgressDialog();

//        click listener to mPasswordLoginButton
        mPasswordLoginButton.setOnClickListener(this);

        //auth state listener
        mAuthListener = new FirebaseAuth.AuthStateListener() {

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // go to user account fragment
                    UserAccountFragment userAccountFragment = new UserAccountFragment();
                    if (mFragmentNavigation != null) {
                        mFragmentNavigation.pushFragment(userAccountFragment);
                    }
                }
            }
        };

        preferences = getActivity().getSharedPreferences(getContext().getPackageName(), Context.MODE_PRIVATE);
    }
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mRegisterTextView) {
            // go to registration fragment
            RegistrationFragment registrationFragment = new RegistrationFragment();
            if (mFragmentNavigation != null) {
                mFragmentNavigation.pushFragment(registrationFragment);
            }
        }
        if (view == mPasswordLoginButton) {
            loginWithPassword();
        }
    }
    private void createAuthProgressDialog() {
        mAuthProgressDialog = new ProgressDialog(getContext());
        mAuthProgressDialog.setTitle("Loading...");
        mAuthProgressDialog.setMessage("Authenticating with Firebase...");
        mAuthProgressDialog.setCancelable(false);
    }
    //    Firebase and authenticating the account with the user-provided credentials
    private void loginWithPassword() {
        String email = mEmailEditText.getText().toString().trim();
        String password = mPasswordEditText.getText().toString().trim();
        if (!Validation.isValidEmail(email)) {
            mEmailEditText.setError("Please enter correct email");
            return;
        } else {
            mEmailAdrress = email;
        }

        if (!Validation.isValidPassword(password)) {
            mPasswordEditText.setError("Password cannot be blank");
            return;
        } else {
            mPassword = password;
        }
        /**
         *  adding onComplete listener
         *  -is responsible for determining when the authentication attempt is complete,
         *  and executes the onComplete() override when it is
         */
        mAuthProgressDialog.show();
        mAuth.signInWithEmailAndPassword(mEmailAdrress, mPassword)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG,"singInWithEmail:onComplete:" + task.isSuccessful());
                        mAuthProgressDialog.dismiss();

                        createUserProfile(task.getResult().getUser());
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail", task.getException());
                            Toast.makeText(getActivity(), "Authentication failed.", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                });
    }

    // creating firebase user name (user profile)
    private void createUserProfile(final FirebaseUser user) {

        Log.d(TAG, user.getDisplayName());
        LoginUtils.setLoggedInUser(user.getDisplayName(), preferences, getContext());
    }
}
