package com.example.dickson.eatinn.network;

import com.example.dickson.eatinn.BuildConfig;

/**
 * Created by dickson-incentro on 2/26/18.
 */

public class NetworkVariables {
    private static final String YELP_CONSUMER_KEY = BuildConfig.YELP_CONSUMER_KEY;
    private static final String YELP_CONSUMER_SECRET = BuildConfig.YELP_CONSUMER_SECRET;
    private static final String YELP_TOKEN = BuildConfig.YELP_TOKEN;
    private static final String YELP_BASE_URL = "https://api.yelp.com/v3/businesses/search?term=food";

    public static String getBaseUrl() {
        return YELP_BASE_URL;
    }

    public static String getUrlWithSearchTerm(String location) {

        return getBaseUrl() + "&location=" + location.trim();
    }

    public static String getYelpToken() {
        return YELP_TOKEN;
    }
}
