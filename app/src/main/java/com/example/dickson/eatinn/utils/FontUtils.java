package com.example.dickson.eatinn.utils;

import android.app.Activity;
import android.graphics.Typeface;

/**
 * Created by dickson-incentro on 2/5/18.
 */

public class FontUtils {

    public static Typeface getMuseo_500(Activity activity) {
        return Typeface.createFromAsset(activity.getAssets(), "fonts/museo_500.otf");
    }

    public static Typeface getMuseo_300(Activity activity) {
        return Typeface.createFromAsset(activity.getAssets(), "fonts/museo_300.otf");
    }

    public static Typeface getMuseo_700(Activity activity) {
        return Typeface.createFromAsset(activity.getAssets(), "fonts/museo_700.otf");
    }

    public static Typeface getMuseo_900(Activity activity) {
        return Typeface.createFromAsset(activity.getAssets(), "fonts/museo_900.otf");
    }

    public static Typeface getGunplay(Activity activity) {
        return Typeface.createFromAsset(activity.getAssets(), "fonts/gunplay.ttf");
    }
}
