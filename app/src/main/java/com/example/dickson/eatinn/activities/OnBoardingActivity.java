package com.example.dickson.eatinn.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.dickson.eatinn.R;
import com.example.dickson.eatinn.fragments.OnboardingFragment;
import com.example.dickson.eatinn.utils.GlobalUtils;
import com.example.dickson.eatinn.variables.PreferenceVariables;

import java.util.ArrayList;
import java.util.List;

public class OnBoardingActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener, View.OnTouchListener {

    // Amount of slides in the viewpager, increase when more pages must be added
    private final int PAGE_AMOUNT = 2;

    // Version of the viewpager, is new screens are added and it must be showed to everyone, increase number
    public static final int ONBOARDING_VERSION = 1;

    private SharedPreferences preferences;
    private OnBoardingPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private List<ImageView> pagerList;
    private LinearLayout pagerStrip;
    private HorizontalScrollView scrollView;
    private ImageView backgroundImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);

        preferences = this.getSharedPreferences(getApplicationContext().getPackageName(), Context.MODE_PRIVATE);

        // init views
        Button button = findViewById(R.id.action_onboarding);
        mViewPager = findViewById(R.id.viewpager);
        scrollView = findViewById(R.id.horizontal_sv);
        backgroundImage = findViewById(R.id.background_image);
        pagerStrip = findViewById(R.id.pager_strip);
        TextView title = findViewById(R.id.title);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new OnBoardingPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(this);
        mViewPager.setPageTransformer(true, new OnboardingPageTransformer());

        // create the dots (pager tab strip)
        fillPagerStrip();

        // set fonts
//        button.setTypeface(FontUtils.getMuseo_500(OnBoardingActivity.this));
//        title.setTypeface(FontUtils.getMuseo_700(OnBoardingActivity.this));

        // set listeners
        button.setOnClickListener(this);
        scrollView.setOnTouchListener(this);
    }

    /**
     * for each page in the adapter, add an imageview with a background to show progress
     */
    private void fillPagerStrip() {
        pagerList = new ArrayList<>();
        int margins = GlobalUtils.dipToPixels(getApplicationContext(), 2);

        if(mSectionsPagerAdapter.getCount() > 1) {
            for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
                AppCompatImageView img = new AppCompatImageView(getApplicationContext());

                if (i == 0) {
                    img.setImageResource(R.drawable.viewpager_paging_selected);
                } else {
                    img.setImageResource(R.drawable.viewpager_paging_unselected);
                }

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.setMargins(margins, 0, margins, 0);
                img.setLayoutParams(lp);

                pagerList.add(img);
                pagerStrip.addView(img);
            }
        }
        else{
            pagerStrip.setVisibility(View.GONE);

            // scroll the view to the middle because there is only 1 page
            ViewTreeObserver viewTree = backgroundImage.getViewTreeObserver();
            viewTree.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {
                    // get the width from the image
                    int backgroundImageWidth = backgroundImage.getMeasuredWidth();
                    int scrollViewWidth = scrollView.getMeasuredWidth();

                    // scroll to the middle
                    int scrollTo = (backgroundImageWidth - scrollViewWidth) / 2;
                    scrollView.scrollTo(scrollTo, 0);

                    return true;
                }
            });
        }
    }

    /**
     * onClick events
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.action_onboarding:
                // set the onboarding preference to the current onboarding value
                preferences.edit().putInt(PreferenceVariables.ONBOARDING_VERSION_SEEN, 5).apply();

                //finish the page
                finish();
                break;
        }
    }

    /**
     * disable the scrollview
     * @param view
     * @param motionEvent
     * @return
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return true;
    }


    /**
     * When the viewpager is scrolled, calculate how much the background must be scrolled
     * @param position
     * @param positionOffset
     * @param positionOffsetPixels
     */
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        int x = (int) ((mViewPager.getWidth() * position + positionOffsetPixels) * computeFactor());
        scrollView.scrollTo(x, 0);
    }

    /**
     * Calculate a parallax factor
     * @return
     */
    private float computeFactor() {
        return (backgroundImage.getWidth() - mViewPager.getWidth()) /
                (float)(mViewPager.getWidth() * (mViewPager.getAdapter().getCount() - 1));
    }

    /**
     * when a new page is selected
     * set the pager icon
     * set the buttons based on position
     * @param position
     */
    @Override
    public void onPageSelected(int position) {

        // set current pager icon
        for (int i = 0; i < pagerList.size(); i++) {
            if(i == position) {
                pagerList.get(i).setImageResource(R.drawable.viewpager_paging_selected);
            }
            else {
                pagerList.get(i).setImageResource(R.drawable.viewpager_paging_unselected);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * Viewpager Adapter
     */
    public class OnBoardingPagerAdapter extends FragmentPagerAdapter {

        public OnBoardingPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return OnboardingFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return PAGE_AMOUNT;
        }
    }

    public class OnboardingPageTransformer implements ViewPager.PageTransformer {

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();
            TextView content = view.findViewById(R.id.content);
            ImageView image = view.findViewById(R.id.image);

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 1) { // [-1,1]
                content.setTranslationX((position) * (pageWidth / 3.2f));
                image.setTranslationX((position) * (pageWidth / 1.1f));

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }
}
