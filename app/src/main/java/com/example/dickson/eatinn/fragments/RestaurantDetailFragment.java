package com.example.dickson.eatinn.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dickson.eatinn.R;

/**
 * Created by dickson-incentro on 3/17/18.
 */

public class RestaurantDetailFragment extends BaseFragment {
    private final String TAG = RestaurantDetailFragment.class.getSimpleName();

    private final static String ARG_PARAM_0 = "title";

    private String restaurantTitle;

    public static RestaurantDetailFragment newInstance(String title) {
        RestaurantDetailFragment fragment = new RestaurantDetailFragment();

        Bundle args = new Bundle();
        args.putString(ARG_PARAM_0, title);

        return fragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            restaurantTitle = args.getString(ARG_PARAM_0);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_restaurant_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // set tool bar with title
        // setupToolBar(view, true, restaurantTitle);
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
