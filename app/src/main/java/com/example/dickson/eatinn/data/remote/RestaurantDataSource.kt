package com.example.dickson.eatinn.data.remote

import com.example.dickson.eatinn.data.Restaurant
import com.example.dickson.eatinn.objects.RestaurantObject

/**
 * Main entry point for accessing restaurant data.
 *
 *
 * should add callbacks to other methods to inform the user of network/database errors or successful operations.
 * usually every operation on database or network should be executed in a different thread.
 */
interface RestaurantDataSource {

    interface LoadRestaurantsCallback {
        fun onRestaurantsLoaded(restaurants: List<Restaurant>)
        fun onDataNotAvailable()
    }

    interface GetRestaurantCallback {

        fun onRestaurantLoaded(restaurant: Restaurant)

        fun onDataNotAvailable()
    }

    interface CheckIfRestaurantStarredCallback {
        fun onRestaurantStarred(starred: Boolean)
    }

    fun getRestaurants(callback: LoadRestaurantsCallback)
    fun getStarredRestaurants(callback: LoadRestaurantsCallback)
    fun getRestaurant(id: String, callback: GetRestaurantCallback)
    fun saveRestaurant(restaurant: Restaurant)
    fun updateRestaurant(restaurant: Restaurant)
//    fun starRestaurant(id: String, starred: Boolean)
//    fun isRestaurantStared(restaurantId: String, callback: CheckIfRestaurantStarredCallback)
}
