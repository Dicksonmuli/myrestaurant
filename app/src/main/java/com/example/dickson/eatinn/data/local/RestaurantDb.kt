package com.example.dickson.eatinn.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.example.dickson.eatinn.data.Restaurant

/**
 * The Room Database that contains the restaurant table.
 */
@Database(entities = arrayOf(Restaurant::class), version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class RestaurantDb : RoomDatabase() {
    abstract fun restaurantDao(): RestaurantDao

    companion object {
        private var INSTANCE: RestaurantDb? = null

        private val lock = Any()

        @JvmStatic
        fun getInstance(context: Context): RestaurantDb {

            // When calling this instance concurrently from multiple threads we're in serious trouble:
            // So we use this method, 'sychronized to lock the instance
            synchronized(lock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, RestaurantDb::class.java, "restaurant.db").build()
                }
                return INSTANCE!!
            }
        }
    }
}