package com.example.dickson.eatinn.interfaces.callbacks;

/**
 * Created by dickson-incentro on 2/19/18.
 */

public interface MainActivityCallback {

    /**
     * Open tab
     * @param tabNumber which tab to open by number
     */
    void openTab(int tabNumber);

    /**
     * Open search activity
     * @param hasOptionsMenu hasOptionsMenu
     */
    void openSearch(boolean hasOptionsMenu);
}
