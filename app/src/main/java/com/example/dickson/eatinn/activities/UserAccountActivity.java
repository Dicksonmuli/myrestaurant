package com.example.dickson.eatinn.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.dickson.eatinn.R;
import com.example.dickson.eatinn.fragments.UserAccountFragment;
import com.example.dickson.eatinn.interfaces.NavigationInteraction;
import com.example.dickson.eatinn.navigation.FragNavController;
import com.example.dickson.eatinn.utils.LoginUtils;
import com.example.dickson.eatinn.variables.GlobalVariables;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserAccountActivity extends AppCompatActivity implements NavigationInteraction, FragNavController.RootFragmentListener{

    //tag for logging
    protected final String TAG = UserAccountActivity.class.getSimpleName();

    private FragNavController mNavController;

    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_account);

        //Setup navigation
        mNavController = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.content) //Initialize FragmentManager and fragment container
                .rootFragmentListener(this, 2) //2 is the number of fragments to be switched between
                .build(); //return FragNavController instance

        preferences = this.getSharedPreferences(this.getApplicationContext().getPackageName(), Context.MODE_PRIVATE);

    }

    /**
     * Add a fragment to the stack
     */
    @Override
    public void pushFragment(Fragment fragment) {
        if (!isFinishing() && mNavController != null) {
            mNavController.pushFragment(fragment);
        }
    }

    @Override
    public void popFragment() {
        if (!isFinishing() && mNavController != null && !mNavController.isRootFragment()) {
            mNavController.popFragment();
        }
    }

    @Override
    public void replaceFragment(Fragment fragment) {
        throw new UnsupportedOperationException("needs implementing");
    }

    @Override
    public Fragment getCurrentFragment() {
        return null;
    }

    @Override
    public void finishActivity() {
        setReturnIntent();
        finish();
    }

    private void setReturnIntent(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra(GlobalVariables.INTENT_IS_LOGGED_IN, LoginUtils.isLoggedIn(preferences));
        setResult(Activity.RESULT_OK, returnIntent);
    }

    /**
     * Return root fragment
     */
    @Override
    public Fragment getRootFragment(int index) {
        return new UserAccountFragment();
    }

    /**
     * Method onBackPressed, handling the 'physical' back button
     */
    @Override
    public void onBackPressed() {
        if (!mNavController.isRootFragment()) {
            mNavController.popFragment();
        } else {
            setReturnIntent();
            super.onBackPressed();
        }
    }

    /**
     * Method onOptionsItemSelected, handling the 'toolbar' back button
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
