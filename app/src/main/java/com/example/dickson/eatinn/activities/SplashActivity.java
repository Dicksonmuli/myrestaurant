package com.example.dickson.eatinn.activities;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.AnticipateInterpolator;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.dickson.eatinn.R;
import com.example.dickson.eatinn.variables.GlobalVariables;
import com.example.dickson.eatinn.variables.PreferenceVariables;

/**
 * <b>Splash Activity</b>
 *
 * Created by dickson-incentro on 2/10/18.
 */

public class SplashActivity extends AppCompatActivity{
    private String TAG = getClass().getSimpleName();
    private SharedPreferences prefs;
    private Handler handler;
    private ImageView logo;
    private boolean handlerFinished;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = this.getSharedPreferences(getApplicationContext().getPackageName(), Context.MODE_PRIVATE);

        if (GlobalVariables.ERASE_PREFS) {
            PreferenceVariables.removePreferencesFromDevice(getApplicationContext());
        }

        setContentView(R.layout.activity_splash);

        logo = findViewById(R.id.logo);
//        getAppContent();
    }

    /**
     * When activity stops delete the callback
     */
    @Override
    protected void onStop() {

        if (handler != null) {
            handler.removeCallbacks(runnable);
        }

        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // start handler
        handler = new android.os.Handler();
        handler.postDelayed(runnable, 4000);
    }

    /**
     * Runnable for going to next page
     */
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            handlerFinished = true;
            nextPage();
        }
    };

    /**
     * Go to the next page
     */
    private void nextPage() {
        Log.d(TAG, "show next page. Handler finished: " + handlerFinished);

        // go to next page if both are finished so it doesn't go away to quickly
        if(handlerFinished) {

            // start animation
            YoYo.with(Techniques.ZoomOut).duration(400).interpolate(new AnticipateInterpolator()).onEnd(new YoYo.AnimatorCallback() {
                @Override
                public void call(Animator animator) {
                    // start the mainactivity
                    Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class); //use for normal app

                    //Intent mainIntent = new Intent(SplashActivity.this, TestActivity.class); //use for normal app
                    startActivity(mainIntent);

                    // if the onboarding has to be showed, start the onboardingActivity
                    if(prefs.getInt(PreferenceVariables.ONBOARDING_VERSION_SEEN, 0) < OnBoardingActivity.ONBOARDING_VERSION ) {
                        Intent onboardIntent = new Intent(SplashActivity.this, OnBoardingActivity.class);
                        startActivity(onboardIntent);
                    }

                    finish();
                }
            }).playOn(logo);
        }
    }
}
