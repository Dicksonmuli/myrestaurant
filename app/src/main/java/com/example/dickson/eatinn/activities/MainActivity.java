package com.example.dickson.eatinn.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.dickson.eatinn.R;
import com.example.dickson.eatinn.fragments.HomeFragment;
import com.example.dickson.eatinn.fragments.LoginFragment;
import com.example.dickson.eatinn.fragments.WishListFragment;
import com.example.dickson.eatinn.interfaces.NavigationInteraction;
import com.example.dickson.eatinn.interfaces.callbacks.MainActivityCallback;
import com.example.dickson.eatinn.navigation.FragNavController;
import com.example.dickson.eatinn.navigation.FragmentHistory;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, MainActivityCallback, NavigationInteraction, FragNavController.TransactionListener, FragNavController.RootFragmentListener{

    public static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Icons for the navigation tabs
     */
    private int[] mTabIconsSelected = {
            R.mipmap.ic_store_black_28dp,
            R.mipmap.ic_favourite_black_24dp,
            R.mipmap.ic_account_black_28dp};

    /**
     * Names for the navigation tabs
     */
    private String[] TABS;

    private CoordinatorLayout coordinator;
    private TabLayout bottomTabLayout;
    private FragNavController mNavController;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FragmentHistory fragmentHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    getSupportActionBar().setTitle("Welcome, " + user.getDisplayName() + "!");
                }else {

                }
            }
        };

        //Obtaining views
        coordinator = findViewById(R.id.content);
        bottomTabLayout = findViewById(R.id.bottom_navigation);
        TABS = getResources().getStringArray(R.array.main_menu);

        //Setup navigation
        fragmentHistory = new FragmentHistory();
        mNavController = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.content)
                .transactionListener(this)
                .rootFragmentListener(this, TABS.length)
                .build();
        switchTab(0);

        //Listener for changing tabs
        bottomTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                fragmentHistory.push(tab.getPosition());
                switchTab(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                mNavController.clearStack();
                switchTab(tab.getPosition());
            }
        });

        //Initialize tabs
        initTab();
    }

    /**
     * Method to initialise tabs
     */
    private void initTab() {
        if (bottomTabLayout != null) {
            for (int i = 0; i < TABS.length; i++) {
                bottomTabLayout.addTab(bottomTabLayout.newTab());
                TabLayout.Tab tab = bottomTabLayout.getTabAt(i);
                if (tab != null) {
                    tab.setCustomView(getTabView(i));
                }

            }
        }
    }

    /**
     * Obtain the tab view
     *
     * @param position position
     * @return View view
     */
    private View getTabView(int position) {
        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.view_tab_item_bottom, null);
        AppCompatImageView icon = view.findViewById(R.id.tab_icon);
        TextView text = view.findViewById(R.id.tab_text);
        icon.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), mTabIconsSelected[position]));
        text.setText(TABS[position]);
        return view;
    }

    @Override
    public void onClick(View view) {

    }

    /**
     * Method to switch tab
     *
     * @param position position
     */
    private void switchTab(int position) {
        mNavController.switchTab(position);
    }

    /**
     * Method onBackPressed, handling the 'physical' back button
     */
    @Override
    public void onBackPressed() {
        if (!mNavController.isRootFragment()) {
            mNavController.popFragment();
        } else {
            if (fragmentHistory.isEmpty()) {
                super.onBackPressed();
            } else {
                if (fragmentHistory.getStackSize() > 1) {
                    int position = fragmentHistory.popPrevious();
                    switchTab(position);
                    updateTabSelection(position);
                } else {
                    switchTab(0);
                    updateTabSelection(0);
                    fragmentHistory.emptyStack();
                }
            }
        }
    }

    /**
     * Method to change which tab is selected
     *
     * @param currentTab currentTab
     */
    private void updateTabSelection(int currentTab) {
        for (int i = 0; i < TABS.length; i++) {
            TabLayout.Tab selectedTab = bottomTabLayout.getTabAt(i);
            assert selectedTab != null;
            if (selectedTab.getCustomView() != null) {
                if (currentTab != i) {
                    selectedTab.getCustomView().setSelected(false);
                } else {
                    selectedTab.getCustomView().setSelected(true);
                }
            }
        }
    }

    /**
     * Add a fragment to the stack
     *
     * @param fragment fragment
     */
    @Override
    public void pushFragment(Fragment fragment) {
        if (!isFinishing() && mNavController != null) {
            mNavController.pushFragment(fragment);
        }
    }

    @Override
    public void popFragment() {
        if (!isFinishing() && mNavController != null && !mNavController.isRootFragment()) {
            mNavController.popFragment();
        }
    }

    @Override
    public void replaceFragment(Fragment fragment) {
        if (!isFinishing() && mNavController != null) {
            mNavController.replaceFragment(fragment);
        }
    }

    @Override
    public Fragment getCurrentFragment() {
        return null;
    }

    @Override
    public void finishActivity() {
        throw new UnsupportedOperationException("Needs implementing");
    }

    @Override
    public void onTabTransaction(Fragment fragment, int index) {
        //If we have a back stack, show the back button
        if (getSupportActionBar() != null && mNavController != null) {
        }
    }

    @Override
    public void onFragmentTransaction(Fragment fragment, FragNavController.TransactionType transactionType) {
        //do fragment stuff. Maybe change title, I'm not going to tell you how to live your life
        //If we have a back stack, show the back button
        if (getSupportActionBar() != null && mNavController != null) {
        }
    }

    /**
     * Obtain the right fragment for the right tab
     *
     * @param index the index that the root of the stack Fragment needs to go
     * @return Fragment
     */
    @Override
    public Fragment getRootFragment(int index) {
        switch (index) {
            case FragNavController.TAB1:
                return new HomeFragment();
            case FragNavController.TAB2:
                return new WishListFragment();
            case FragNavController.TAB3:
                return new LoginFragment();
        }

        throw new IllegalStateException("Need to send an index that we know.");
    }

    @Override
    public void openTab(int tabNumber) {
        try {
            bottomTabLayout.getTabAt(tabNumber).select();
        } catch (NullPointerException e) {
            Log.e(TAG, e.toString());
        }
    }


    @Override
    public void openSearch(boolean hasOptionsMenu) {

    }
}
