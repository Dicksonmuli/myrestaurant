package com.example.dickson.eatinn.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.dickson.eatinn.R;
import com.example.dickson.eatinn.interfaces.NavigationInteraction;
import com.example.dickson.eatinn.interfaces.callbacks.MainActivityCallback;
import com.example.dickson.eatinn.utils.GlobalUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {

    public NavigationInteraction mFragmentNavigation;
    public MainActivityCallback mMainActivityCallback;
    public static final String TAG = "BaseFragment";
    // init views
    private TextView titleTv, subTitleTv;

    private SharedPreferences preferences;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = getActivity().getSharedPreferences(getContext().getApplicationContext().getPackageName(), Context.MODE_PRIVATE);
    }

    /**
     * onAttach method
     * @param context context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //parse context to FragmentNavigation
        if (context instanceof NavigationInteraction) {
            mFragmentNavigation = (NavigationInteraction) context;
        }

        if (context instanceof MainActivityCallback) {
            mMainActivityCallback = (MainActivityCallback) context;
        }
    }

    /**
     * onDetach
     */
    @Override
    public void onDetach() {
        mFragmentNavigation = null;
        mMainActivityCallback = null;
        super.onDetach();
    }

    /**
     * Method to setup the toolbar if supported.
     *
     * @param view view
     * @param homeAsUpEnabled whether or not home as up is enabled
     */
    public Toolbar setupToolBar(View view, boolean homeAsUpEnabled) {
        //setup toolbar
        Toolbar toolbar = view.findViewById(R.id.toolbar);

        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        activity.setSupportActionBar(toolbar);

        ActionBar actionBar = activity.getSupportActionBar();
        if(actionBar != null) {
            //settings for actionbar
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(homeAsUpEnabled);
        }

        LinearLayout textContainer = toolbar.findViewById(R.id.toolbar_text_container);

        if(homeAsUpEnabled && textContainer != null) {
            textContainer.setPadding(0, 0, GlobalUtils.dipToPixels(getContext(), 16f), 0);
        }

        return toolbar;
    }

    /**
     * Method to setup toolbar with a title, uses other setupToolBar function to do the rest.
     *
     * @param view view
     * @param homeAsUpEnabled whether or not home as up is enabled
     * @param title title
     */
    public Toolbar setupToolBar(View view, boolean homeAsUpEnabled, String title) {
        //setup toolbar and obtain it
        Toolbar toolbar = setupToolBar(view, homeAsUpEnabled);

        //obtain the views of title
        titleTv = toolbar.findViewById(R.id.toolbar_title);

        if(titleTv != null) {
            //get and set font
//            Typeface museo700 = FontUtils.getMuseo_700(getActivity());
//            titleTv.setTypeface(museo700);

            //set title
            titleTv.setText(title);
        }

        return toolbar;
    }

    /**
     * Method to setup toolbar with subtitle, uses other setupToolBar functions to do the rest.
     *
     * @param view view
     * @param homeAsUpEnabled whether or not home as up is enabled
     * @param title title
     * @param subtitle subtitle
     */
    public void setupToolBar(View view, boolean homeAsUpEnabled, String title, String subtitle) {
        //setup for title
        Toolbar toolbar = setupToolBar(view, homeAsUpEnabled, title);

        //obtain the views of subtitle
        subTitleTv = toolbar.findViewById(R.id.toolbar_subtitle);

        if(subTitleTv != null) {
            //set subtitle
            subTitleTv.setText(subtitle);
        }
    }

    /**
     * Method to update the toolbar title
     * @param title title to update to
     */
    public void updateToolbarTitle(String title) {
        if(titleTv != null) {
            titleTv.setText(title);
        }
    }

    /**
     * Method to update the toolbar subtitle
     * @param subTitle subtitle to update to
     */
    public void updateToolbarSubTitle(String subTitle) {
        if(subTitleTv != null) {
            subTitleTv.setText(subTitle);
        }
    }


}
