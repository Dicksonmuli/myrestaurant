package com.example.dickson.eatinn.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.example.dickson.eatinn.R;
import com.example.dickson.eatinn.adapters.RestaurantListAdapter;
import com.example.dickson.eatinn.data.Restaurant;
import com.example.dickson.eatinn.data.local.RestaurantDb;
import com.example.dickson.eatinn.data.local.RestaurantLocalDataSource;
import com.example.dickson.eatinn.data.remote.RestaurantDataSource;
import com.example.dickson.eatinn.data.remote.RestaurantRemoteDataSource;
import com.example.dickson.eatinn.data.remote.RestaurantRepository;
import com.example.dickson.eatinn.objects.RestaurantObject;
import com.example.dickson.eatinn.others.AppExecutors;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {
    private final String TAG = HomeFragment.class.getSimpleName();

    // vieww, adapters and dataset
    private RestaurantListAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private List<Restaurant> mRestaurantList;
    private FrameLayout mProgressBar;
    private RestaurantRepository mRepository;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupToolBar(view, false, "Home");

        // init
        mProgressBar = view.findViewById(R.id.progressBar);

        // Initial configuration and setup
        mRecyclerView = view.findViewById(R.id.recycler_restaurant);
        LinearLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(mAdapter);

        mRepository = RestaurantRepository.getInstance(
                new RestaurantRemoteDataSource(getContext()),
                RestaurantLocalDataSource.getInstance(new AppExecutors(), RestaurantDb.getInstance(getContext()).restaurantDao())
        );

        // if restaurant list is null, create new list and get the restaurants
        if (mRestaurantList == null || mRestaurantList.size() == 0) {
            mRestaurantList = new ArrayList<>();

            getRestaurants();
        } else {
            // hide the progress bar if restaurants are loaded
            mProgressBar.setVisibility(View.GONE);

        }
    }

    /**
     * OnResume Method
     */
    @Override
    public void onResume() {
        super.onResume();

        // restaurants can be displayed from previous load
        if (mAdapter != null){
            // hide progress bar if the adapter is not null
            mProgressBar.setVisibility(View.GONE);

            // notify on data change if adapter is not null
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * inflate overflow menu in the main activity
     *
     * @param menu menu
     * @param inflater inflater
     * @return super method
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Method for action on item selected in the menu
     * @param item item
     * @return super method
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                logout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Method to logout the user
     */
    private void logout() {
        // FirebaseAuth.getInstance().signOut();

        // returning to login activity after the user logs out
    }

    /**
     * Method to do a webcall to fetch restaurants
     */
    private void getRestaurants() {

        // show progress bar
        mProgressBar.setVisibility(View.VISIBLE);

        mRepository.getRestaurants(new RestaurantDataSource.LoadRestaurantsCallback() {
            @Override
            public void onRestaurantsLoaded(@NotNull List<Restaurant> restaurants) {
                mRestaurantList = restaurants;

                Collections.addAll(mRestaurantList);

                // Hide progress bar
                mProgressBar.setVisibility(View.GONE);

                // attach adapter
                if (mAdapter == null) {
                    mAdapter = new RestaurantListAdapter(getActivity(), mFragmentNavigation, mRestaurantList);
                    mRecyclerView.setAdapter(mAdapter);
                } else {
                    // notify on data change if adapter is not null
                    mAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onDataNotAvailable() {

            }
        });

//        String url = NetworkVariables.getUrlWithSearchTerm("london");
//        NetworkManager.getInstance(getContext()).getCall(url, null, new VolleyCallback<String>() {
//            @Override
//            public void getResult(String response, VolleyError error, String errorMessage) {
//                if (!TextUtils.isEmpty(response)) {
//                    parseResults(response);
//                } else {
//                    Toast.makeText(getContext(), "There was an error in the network", Toast.LENGTH_SHORT).show();
//                }
//
//                // Webcall is complete, hide th progress bar
//                mProgressBar.setVisibility(View.GONE);
//            }
//        });
    }

    /**
     * Method to parse the results from a webcall and set adapter
     *
     * @param response response
     */
//    private void parseResults(String response) {
//        if (isAdded()) {
//            try {
//                ObjectMapper objectMapper = new ObjectMapper();
//                JsonNode object = objectMapper.readTree(response);
//
//                if (object.get("businesses") != null) {
//                    JsonNode restaurantObject = object.get("businesses");
//
//                    RestaurantObject[] result = objectMapper.treeToValue(restaurantObject, RestaurantObject[].class);
//                    Collections.addAll(mRestaurantList, result);
//                }
//            } catch (IOException e) {
//                Log.e(TAG, "error: " + e.toString());
//            }
//
//            // attach adapter
//            if (mAdapter == null) {
//                mAdapter = new RestaurantListAdapter(getActivity(), mFragmentNavigation, mRestaurantList);
//                mRecyclerView.setAdapter(mAdapter);
//            } else {
//                // notify on data change if adapter is not null
//                mAdapter.notifyDataSetChanged();
//            }
//        }
//    }

}
