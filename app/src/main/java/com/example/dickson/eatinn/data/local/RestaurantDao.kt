package com.example.dickson.eatinn.data.local

import android.arch.persistence.room.*
import com.example.dickson.eatinn.data.Restaurant

@Dao
interface RestaurantDao {
    /**
     * Select all restaurants from the country table.
     *
     * @return all restaurants.
     */
    @Query("SELECT * FROM restaurants")
    fun getRestaurants(): List<Restaurant>

    /**
     * Select restaurants by location from the restaurants table.
     *
     * @return all countries.
     */
    @Query("SELECT * FROM restaurants WHERE location_city = :location")
    fun getRestaurantsByRegion(location: String): List<Restaurant>

    /**
     * Select restaurants by location from the restaurants table.
     *
     * @return all countries.
     */
    @Query("SELECT * FROM restaurants WHERE id = :id")
    fun getRestaurantById(id: String): Restaurant

    /**
     * Update a country.
     *
     * @param country country to be updated
     * @return the number of countries updated. This should always be 1.
     */
    @Update
    fun updateRestaurant(restaurant: Restaurant): Int

    /**
     * Select all starred restaurants from the restaurant table.
     *
     * @return all countries.
     */
    // SQLite does not have a boolean data type. Room maps it to an INTEGER column, mapping true to 1 and false to 0.
    @Query("SELECT * FROM restaurants WHERE starred = 1 ")
    fun getStarredRestaurants(): List<Restaurant>

    /**
     * Insert a Restaurant in the database. If the Restaurant already exists, replace it.
     *
     * @param country the Restaurant to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertRestaurant(restaurant: Restaurant)

    /**
     * Delete all Restaurant.
     */
    @Query("DELETE FROM restaurants")
    fun deleteRestaurants()

}
