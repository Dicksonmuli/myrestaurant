package com.example.dickson.eatinn.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.dickson.eatinn.R;

/**
 * Created by dickson-incentro on 2/5/18.
 */

public class OnboardingFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";

    private int position;

    public OnboardingFragment() {
        // Required empty public constructor
    }

    /**
     * Method to create a new instance class with title and content.
     */
    public static OnboardingFragment newInstance(int pos) {
        OnboardingFragment fragment = new OnboardingFragment();
        Bundle args = new Bundle();

        args.putInt(ARG_PARAM1, pos);
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * onCreate method
     * @param savedInstanceState savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt(ARG_PARAM1);
        }
    }

    /**
     * onCreateView method
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState savedInstanceState
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_onboarding, container, false);
    }

    /**
     * onViewCreated method
     * @param view view
     * @param savedInstanceState savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // obtain title and content view.
        TextView content = view.findViewById(R.id.content);
        ImageView image = view.findViewById(R.id.image);

        // i
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YoYo.with(Techniques.Shake).duration(700).playOn(view);
            }
        });

        // set details based on position
        switch (position) {
            default:
            case 0:
                content.setText(getString(R.string.onboarding_1_content));
                image.setImageResource(R.mipmap.ic_restaurant_white_24dp);
                break;
            case 1:
                content.setText(getString(R.string.onboarding_1_content));
                image.setImageResource(R.mipmap.ic_restaurant_white_24dp);
                break;

            // ***************
            // README:
            // when new screens are added, add content here
            // ***************
        }

    }

}
