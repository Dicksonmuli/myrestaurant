package com.example.dickson.eatinn.data.remote

import android.util.Log
import com.example.dickson.eatinn.data.Restaurant
import java.util.ArrayList
import java.util.LinkedHashMap

/**
 * Implementation to load restaurants from the data sources into a cache.
 *
 *
 * Implements synchronisation between locally persisted data and data
 * obtained from the server, by using the local data source when remote is not available.
 */
class RestaurantRepository(val restaurantRemoteDataSource: RestaurantDataSource,
                           val restaurantLocalDataSource: RestaurantDataSource) : RestaurantDataSource {

    private val TAG = javaClass.simpleName

    var cachedRestaurants: LinkedHashMap<String, Restaurant> = LinkedHashMap()

    /**
     * Gets restaurants from cache, local data source (SQLite) or remote data source, whichever is
     * available first.
     *
     *
     * Note: [LoadTasksCallback.onDataNotAvailable] is fired if all data sources fail to
     * get the data.
     */
    override fun getRestaurants(callback: RestaurantDataSource.LoadRestaurantsCallback) {

        // Respond immediately with cache if available
        if (cachedRestaurants.isNotEmpty()) {
            callback.onRestaurantsLoaded(ArrayList(cachedRestaurants.values))
            return
        }

        // If the cache is empty we need to fetch new data from the network.
        restaurantRemoteDataSource.getRestaurants(
                object : RestaurantDataSource.LoadRestaurantsCallback {

                    override fun onRestaurantsLoaded(restaurants: List<Restaurant>) {

                        // This will repopulate the cache
                        refreshCache(restaurants)

                        // This will update local database
                        refreshLocalDataSource(restaurants)

                        // Response with data
                        callback.onRestaurantsLoaded(restaurants)
                    }

                    // When data wasn't loaded from remote
                    override fun onDataNotAvailable() {

                        // Query the local storage if available.
                        restaurantLocalDataSource.getRestaurants(object : RestaurantDataSource.LoadRestaurantsCallback {
                            override fun onRestaurantsLoaded(restaurants: List<Restaurant>) {

                                refreshCache(restaurants)

                                callback.onRestaurantsLoaded(ArrayList(restaurants))

                            }

                            override fun onDataNotAvailable() {

                                callback.onDataNotAvailable()
                            }
                        })
                    }

                })
    }

    /**
     * Method to get the starred restaurants only
     */
    override fun getStarredRestaurants(callback: RestaurantDataSource.LoadRestaurantsCallback) {

        restaurantLocalDataSource.getStarredRestaurants(object : RestaurantDataSource.LoadRestaurantsCallback {
            override fun onRestaurantsLoaded(restaurants: List<Restaurant>) {

                callback.onRestaurantsLoaded(ArrayList(restaurants))
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }
        })
    }

    private fun refreshCache(restaurant: List<Restaurant>) {

        cachedRestaurants.clear()

        restaurant.forEach {
            cacheAndPerform(it) {}
        }
    }

    private inline fun cacheAndPerform(restaurant: Restaurant, perform: (Restaurant) -> Unit) {

        cachedRestaurants.put(restaurant.id, restaurant)

        perform(restaurant)

    }

    /**
     * Method to update the local database
     */
    private fun refreshLocalDataSource(restaurants: List<Restaurant>) {
        for (restaurant in restaurants) {

            // Get restaurant by id and update it
            restaurantLocalDataSource.getRestaurant(restaurant.id, object : RestaurantDataSource.GetRestaurantCallback {

                // Country Exists on Database
                override fun onRestaurantLoaded(rest: Restaurant) {

                    rest.name = restaurant.name

                    restaurantLocalDataSource.updateRestaurant(rest)

                }

                // Country Does not exist on Database
                override fun onDataNotAvailable() {

                    // Save Country
                    restaurantLocalDataSource.saveRestaurant(restaurant)
                }

            })
        }
    }

    /**
     * This method can be used if you want to get a certain restaurant by id
     */
    override fun getRestaurant(id: String, callback: RestaurantDataSource.GetRestaurantCallback) {
    }

    /**
     * This method can be used if you want to save a certain restaurant
     */
    override fun saveRestaurant(restaurant: Restaurant) {
    }

    /**
     * This method can be used if you want to update a certain restaurant
     */
    override fun updateRestaurant(restaurant: Restaurant) {
    }


    companion object {

        private var INSTANCE: RestaurantRepository? = null

        /**
         * Returns the single instance of this class, creating it if necessary.

         * @param countryRemoteDataSource the backend data source
         * *
         * @param countryLocalDataSource  the device storage data source
         * *
         * @return the [CountryRepository] instance
         */
        @JvmStatic
        fun getInstance(countryRemoteDataSource: RestaurantDataSource,
                        countryLocalDataSource: RestaurantDataSource) =
                INSTANCE?: synchronized(RestaurantRepository::class.java) {
                    INSTANCE?: RestaurantRepository(countryRemoteDataSource, countryLocalDataSource)
                                    .also { INSTANCE = it }
                }

        /**
         * Used to force [getInstance] to create a new instance
         * next time it's called.
         */
        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }

    }
}