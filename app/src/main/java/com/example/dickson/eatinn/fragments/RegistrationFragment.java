package com.example.dickson.eatinn.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dickson.eatinn.R;
import com.example.dickson.eatinn.utils.LoginUtils;
import com.example.dickson.eatinn.utils.Validation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

/**
 * Created by dickson-incentro on 3/7/18.
 */

public class RegistrationFragment extends BaseFragment implements View.OnClickListener {

    private final String TAG = getClass().getSimpleName();
    FirebaseAuth.AuthStateListener mAuthListener;

    private FirebaseAuth mAuth;
    private Button mCreateUserButton;
    private EditText mNameEditText;
    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private EditText mConfirmPasswordEditText;
    private TextView mLoginTextView;

    private ProgressDialog mAuthProgressDialog;
    private String mName;

    private SharedPreferences preferences;
    /**
     * OnCreateView, inflates layout
     *
     * @param inflater           inflater
     * @param container          container
     * @param savedInstanceState savedInstanceState
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_registration, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mNameEditText = view.findViewById(R.id.name);
        mEmailEditText = view.findViewById(R.id.email);
        mPasswordEditText = view.findViewById(R.id.password);
        mConfirmPasswordEditText = view.findViewById(R.id.password_confirm);

        mLoginTextView = view.findViewById(R.id.goToLoginTextView);
        mCreateUserButton = view.findViewById(R.id.create_account_button);

        createAuthStateListener();
        createAuthProgressDialog();

        mAuth = FirebaseAuth.getInstance();
        mLoginTextView.setOnClickListener(this);
        mCreateUserButton.setOnClickListener(this);

        preferences = getActivity().getSharedPreferences(getContext().getPackageName(), Context.MODE_PRIVATE);
    }
    @Override
    public void onStart() {
        super.onStart();
        try {
            mAuth.addAuthStateListener(mAuthListener);
        }catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    //progress dialog method for authenticating with firebase
    private void createAuthProgressDialog() {
        mAuthProgressDialog = new ProgressDialog(getContext());
        mAuthProgressDialog.setTitle("Loading...");
        mAuthProgressDialog.setMessage("Authenticating with database...");
        mAuthProgressDialog.setCancelable(false);
    }
    @Override
    public void onClick(View view) {
        if (view == mLoginTextView) {
            // go to registration fragment
            LoginFragment registrationFragment = new LoginFragment();
            if (mFragmentNavigation != null) {
                mFragmentNavigation.pushFragment(registrationFragment);
            }
        }
        if (view == mCreateUserButton) {
            createNewUser();
        }
    }
    //    creating new user and adding email and password on firebase
    private void createNewUser() {
        final String name = mNameEditText.getText().toString().trim();
        final String email = mEmailEditText.getText().toString().trim();
        String password = mPasswordEditText.getText().toString().trim();
        String confirmPassword = mConfirmPasswordEditText.getText().toString().trim();
        mName = mNameEditText.getText().toString().trim();

        if (!Validation.isValidEmail(email)) {
            mEmailEditText.setError("Please enter a valid email address");
            return;
        }
        if (!Validation.isValidName(mName)) {
            mNameEditText.setError("Please enter your name");
        }
        if (!Validation.isValidPassword(password, confirmPassword)) {
            mPasswordEditText.setError("Passwords error");
        }

        mAuthProgressDialog.show();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        mAuthProgressDialog.dismiss();
                        if (task.isSuccessful()) {
                            Toast.makeText(getActivity(), "Authentication was Successful.",
                                    Toast.LENGTH_SHORT).show();
                            createFirebaseUserProfile(task.getResult().getUser());
                        } else {
                            Toast.makeText(getActivity(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    // creating user authentication listener
    private void createAuthStateListener() {
        mAuthListener = new FirebaseAuth.AuthStateListener() {

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // go to user account fragment
                    UserAccountFragment userAccountFragment = new UserAccountFragment();
                    if (mFragmentNavigation != null) {
                        mFragmentNavigation.pushFragment(userAccountFragment);
                    }
                }
            }

        };
    }
    // creating firebase user name (user profile)
    private void createFirebaseUserProfile(final FirebaseUser user) {

        UserProfileChangeRequest addProfileName = new UserProfileChangeRequest.Builder()
                .setDisplayName(mName)
                .build();

        user.updateProfile(addProfileName)
                .addOnCompleteListener(new OnCompleteListener<Void>() {

                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, user.getDisplayName());
                            LoginUtils.setLoggedInUser(user.getDisplayName(), preferences, getContext());
                        }
                    }

                });
    }
}
