package com.example.dickson.eatinn.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by dickson-incentro on 2/10/18.
 */

public class GlobalUtils {

    private static final String TAG = GlobalUtils.class.getSimpleName();

    public static int dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float pxFloat = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
        return (int) pxFloat;
    }

    public static void hideKeyboard(Activity activity, View viewToRemoveFrom) {
        try {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            InputMethodManager manager = ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE));

            if ((activity.getCurrentFocus() != null) && (activity.getCurrentFocus().getWindowToken() != null)) {
                manager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
            if (viewToRemoveFrom != null && viewToRemoveFrom.getWindowToken() != null) {
                manager.hideSoftInputFromWindow(viewToRemoveFrom.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

}
